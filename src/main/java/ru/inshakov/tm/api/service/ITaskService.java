package ru.inshakov.tm.api.service;

import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    void remove(Task task);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    Task findOneById(String id);

    Task removeOneById(String id);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    void clear();

}
