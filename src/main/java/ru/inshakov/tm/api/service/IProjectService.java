package ru.inshakov.tm.api.service;

import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    Project findOneById(String id);

    Project removeOneById(String id);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    void clear();

}
