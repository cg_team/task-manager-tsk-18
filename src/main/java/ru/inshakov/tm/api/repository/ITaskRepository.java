package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByProjectId(String projectId);

    Task removeOneByProjectId(String projectId);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    List<Task> findAllTasksByProjectId (String projectId);

    List<Task> removeAllTasksByProjectId(String projectId);

    int size();
}
