package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.*;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.exception.user.EmailTakenException;
import ru.inshakov.tm.exception.user.LoginTakenException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService{

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public boolean isLoginExists(String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExists(String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }


    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeById(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginTakenException(login);
        if (isEmailExists(email)) throw new EmailTakenException(email);
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(String userId, String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }
}
