package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.system.UndefinedErrorException;
import ru.inshakov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) throw new UndefinedErrorException();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        //if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
    }

    @Override
    public void removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        remove(project);
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }
    @Override
    public Project changeProjectStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
