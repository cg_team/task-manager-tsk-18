package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }


    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> listOfProjects = new ArrayList<>(projects);
        listOfProjects.sort(comparator);
        return listOfProjects;
    }

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for(final Project project: projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

}
