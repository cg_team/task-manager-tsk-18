package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    public List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(String id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }


    @Override
    public User findByEmail(final String email) {
        for (final User user: users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }


    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
