package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

}
