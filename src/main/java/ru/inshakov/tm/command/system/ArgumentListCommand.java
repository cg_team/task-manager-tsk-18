package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "arg-list";
    }

    @Override
    public String description() {
        return "Show program arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command: commands) {
            final String arg = command.arg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }
}
