package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "App version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("0.0.1");
    }

}
